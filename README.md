# plot-digitizer-ci
Offline, stand-alone versions of [WebPlotDigitizer](https://github.com/ankitrohatgi/WebPlotDigitizer)

There is a scheduled CI job to check for upstream code changes every few hours.  If the master branch of [WebPlotDigitizer](https://github.com/ankitrohatgi/WebPlotDigitizer) has changes, the job will update the submodule to head, and commit back to this repo triggering a build, tag, and release.

## Desktop App (Electron)
Tagged releases are available on the [releases page](https://gitlab.com/dam90/plot-digitizer-ci/-/releases).

The latest portable builds are available below (unless the build failed).
  * Windows: [WebPlotDigitizer.exe](https://gitlab.com/dam90/plot-digitizer-ci/-/jobs/artifacts/master/raw/release/WebPlotDigitizer.exe?job=Windows)
  * Mac OS X: [WebPlotDigitizer.app](https://gitlab.com/dam90/plot-digitizer-ci/-/jobs/artifacts/master/raw/release/WebPlotDigitizer.app.zip?job=Mac)

## Docker
Run a web server with the WebPlotDigitizer application:

  1. Run it:  `docker run -it --rm -p 81:80 registry.gitlab.com/dam90/plot-digitizer-ci/server:latest`
  2. In your browser, visit: http://localhost:81

## Gitlab  Pages
Use it via GitLab pages: https://dam90.gitlab.io/plot-digitizer-ci/

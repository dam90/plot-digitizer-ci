
RELEASE_JSON='{ "name": "'$RELEASE_NAME'", "tag_name": "'$CI_COMMIT_TAG'", "description": "'$RELEASE_DESCRIPTION'", "milestones": [], "assets": { "links": [{ "name": "Windows Executable", "url": "'$WINDOWS_ARTIFACT_URL'" }, { "name": "Mac OS X App", "url": "'$MAC_ARTIFACT_URL'" }] } }'

RELEASE_URL="https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/releases"

echo "---"
echo "JSON data:"
echo $RELEASE_JSON
echo "---"

echo "---"
echo "API URL:"
echo $RELEASE_URL
echo "---"

curl -H 'Content-Type: application/json' -H "PRIVATE-TOKEN: $API_TOKEN" --data "${RELEASE_JSON}" -X POST "$RELEASE_URL"